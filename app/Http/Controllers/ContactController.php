<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostContactForm;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    const MAIL_RECEIPIENTS = [
        'me@example.com'
    ];

    public function sendMessage(PostContactForm $request)
    {
        Mail::to(self::MAIL_RECEIPIENTS)->send( new ContactMail([
            'name'    => $request->post('name'),
            'email'   => $request->post('email'),
            'subject' => $request->post('subject'),
            'message' => $request->post('message'),
        ]));

        return view('contact-us', ['sentMessage' => true]);
    }
}
