<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Basket;
use App\Stock;
use App\Merchandise;

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ($i = 1; $i <= $request->selectQuantity; $i++) {
            $basket = new Basket();
            $basket->unique_id = session()->getId();
            $basket->stock_id = $request->selectStockId;
            $basket->date_created = date('Y-m-d H:i:s');
            $basket->save();
        }
// COMMAND FOR DECREMENTING THE STOCK WHEN IT'S ACTUALLY PURCHASED
//        $firstUser = Stock::find($request->selectStockId);
//        $firstUser->decrement('stock', $request->selectQuantity);

        return self::basketSessionItems();
    }

    public function basketSessionItems(){

        $retrieveBasket = Basket::all()->where('unique_id', '=', session()->getId());
        $basketTotal = 0;
        foreach ($retrieveBasket as $basketItem) {
            $stockID = Stock::select('merchandise_id')->where('id', $basketItem->stock_id)->get();
            $price = Merchandise::select('price')->where('id', $stockID[0]->merchandise_id)->get();
            $basketTotal += $price[0]->price;
        }
        return $basketTotal;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
