<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;


    const NAME = 'no reply';

    /** @var string */
    private $msgName;
    /** @var string */
    private $msgEmail;
    /** @var string */
    private $msgSubject;
    /** @var $message */
    private $msgMessage;

    /**
     * Create a new message instance.
     *
     * @param array $data
     * @return void
     */
    public function __construct($data = [])
    {
        $this->msgName    = $data['name'] ?: null;
        $this->msgEmail   = $data['email'] ?: null;
        $this->msgSubject = $data['subject'] ?: null;
        $this->msgMessage = $data['message'] ?: null;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'name'    => $this->msgName,
            'email'   => $this->msgEmail,
            'subject' => $this->msgSubject,
            'msg' => $this->msgMessage,
        ];

        return $this->view('emails.contact-us', $params)
            ->from($this->msgEmail, self::NAME)
            ->subject($this->msgSubject)
            ->with($this->msgMessage);
    }
}
