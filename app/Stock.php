<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'merchandise_stock';
    public $timestamps = false;

    public function size()
    {
        return $this->hasOne(Size::class, 'id', 'size_id');
    }

    public function updateStockQuantity( $request ){

    }
}
