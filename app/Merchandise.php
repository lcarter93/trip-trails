<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchandise extends Model
{
    protected $table = "Merchandise";

    public function stocks()
    {
        return $this->hasMany(Stock::class, 'merchandise_id', 'id')->where('stock', '>', 0);
    }

    public function stockSizes()
    {
        $stocks = $this->stocks;
        $stockSizes = [];
        foreach($stocks as $stock) {
            $stockSizes[$stock->size->id] = [
                'id'  => $stock->id,
                'name' => $stock->size->size,
                'stock'  => $stock->stock,
            ];
        }
        return $stockSizes;
    }
}
