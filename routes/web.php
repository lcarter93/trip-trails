<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Merchandise;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', 'IndexController@index');
Route::put('basket/store-basket-items', 'BasketController@store');
Route::put('basket/get-basket-session', 'BasketController@basketSessionItems');
Route::get('about-us', function () {
    return view('about-us');
});
Route::get('basket', function () {
    return view('basket');
});
Route::get('events', function () {
    return view('events');
});
Route::get('contact-us', function () {
    return view('contact-us');
});
Route::post('contact-us', 'ContactController@sendMessage')
    ->name('contact-us');
Route::get('merchandise', 'MerchandiseController@index');
Route::get('gallery', 'GalleryController@index');
