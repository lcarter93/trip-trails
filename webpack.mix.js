const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/js/app.js', 'public/js')
//   .sass('resources/sass/app.scss', 'public/css')
mix.copy('resources/impact/js/*', 'public/js')
    .copy('resources/impact/css/*', 'public/css')
    .copy('resources/impact/fonts/*', 'public/fonts')
    .copy('resources/impact/images/', 'public/images', false)
    .sass('resources/sass/triptrails.scss', 'public/css')
    .copy('resources/js/ecommerce/*', 'public/js')
    .copy('resources/js/*', 'public/js')
;
