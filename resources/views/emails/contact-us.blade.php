<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Contact Us Message</h2>

        <p><strong>Name: </strong>{{ $name }}</p>
        <p><strong>Subject: </strong>{{ $subject }}</p>
        <p><strong>Email: </strong>{{ $email }}</p>
        <p><strong>Message: </strong><br />{{ $msg }}</p>
    </body>
</html>