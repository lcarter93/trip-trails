<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Tirpentwys Trails</title>
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pe-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/triptrails.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/notify.js') }}"></script>
        <script src="{{ asset('js/updateBasket.js') }}"></script>


        <!--[if lt IE 9]>
        <script src="{{ asset('js/html5shiv.js') }}"></script>
        <script src="{{ asset('js/respond.min.js') }}"></script>
        <![endif]-->
        <link rel="shortcut icon" href="{{ asset('images/favicon/gearFavIcon_64.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/favicon/gearFavIcon_144.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/favicon/gearFavIcon_114.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/favicon/gearFavIcon_72.png') }}">
        <link rel="apple-touch-icon" href="{{ asset('images/favicon/gearFavIcon_57.png') }}">

        <script type="text/javascript">
            jQuery(document).ready(function($){
                'use strict';
                jQuery('body').backstretch([
                    "{{ asset('images/bg/bg10_13032019.jpg') }}",
                    "{{ asset('images/bg/bg12_13032019.jpg') }}",
                    "{{ asset('images/bg/bg8_13032019.jpg') }}",
                    "{{ asset('images/bg/bg6_13032019.jpg') }}",
                    "{{ asset('images/bg/bg7_13032019.jpg') }}",
                    "{{ asset('images/bg/bg4_13032019.jpg') }}",
                    "{{ asset('images/bg/bg5_13032019.jpg') }}"
                ], {duration: 5000, fade: 500, centeredY: true });

                $("#mapwrapper").gMap({ controls: false,
                    scrollwheel: false,
                    markers: [{
                        latitude:40.7566,
                        longitude: -73.9863,
                        icon: { image: "{{ asset('images/marker.png') }}",
                            iconsize: [44,44],
                            iconanchor: [12,46],
                            infowindowanchor: [12, 0] } }],
                    icon: {
                        image: "{{ asset('images/marker.png') }}",
                        iconsize: [26, 46],
                        iconanchor: [12, 46],
                        infowindowanchor: [12, 0] },
                    latitude:40.7566,
                    longitude: -73.9863,
                    zoom: 14 });
            });
        </script>
    </head>
    <!--/head-->
    <body>
        <div id="preloader"></div>
        <header class="navbar navbar-inverse navbar-fixed-top " role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><h1><span class="pe-7s-gleam bounce-in"></span> Tirpentwys Trails</h1></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('about-us') }}">About Us</a></li>
                        <li><a href="{{ url('events') }}">Events</a></li>
                        <li><a href="{{ url('merchandise') }}">Merchandise</a></li>
                        <li><a href="{{ url('gallery') }}">Gallery</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <!--/header-->
        <div id="content-wrapper">
            @yield('content')
            <div class="shop-basket__container">
                <img src="{{ asset('images/favicon/cartFavicon_32.png') }}" class="favicon">
                Basket Total:
                <a id="basket-total" href="{{ url('basket') }}">
                    £0
                </a>
            </div>
            <footer id="footer" class="">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            &copy; 2019 tirpentwys-trails.com. All Rights Reserved. <a href="https://templatemag.com/bootstrap-templates/">Bootstrap template</a> by TemplateMag.
                        </div>
                        <div class="col-sm-4">
                            <ul class="pull-right">
                                <li><a id="gototop" class="gototop" href="#"><i class="fa fa-chevron-up"></i></a></li><!--#gototop-->
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!--/#footer-->
        </div>

        <script src="js/plugins.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWDPCiH080dNCTYC-uprmLOn2mt2BMSUk&amp;sensor=true"></script>
        <script src="js/init.js"></script>
    </body>
</html>