@extends('layouts.main')

@section('content')

    <section id="single-page-slider" class="no-margin">
        <div class="gap"></div>
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center gap fade-down section-heading">
                                    <h2 class="main-title">Your Basket</h2>
                                    <hr>
                                    <p>Purchase your Tirpentwys Merchandise Below</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->

    <div id="content-wrapper">
        <section id="portfolio" class="white">
                <div class="container">
                    <div class="row margin-bottom-40">
                        <!-- BEGIN CONTENT -->
                        <div class="col-md-12 col-sm-12 fade-down">
                            <h1>Basket Summary</h1>
                            <div class="goods-page">
                                <div class="goods-data clearfix">
                                    <div class="table-wrapper-responsive">
                                        <table summary="Shopping cart">
                                            <tr>
                                                <th class="goods-page-image">Image</th>
                                                <th class="goods-page-description">Description</th>
                                                <th class="goods-page-quantity">Quantity</th>
                                                <th class="goods-page-price">Unit price</th>
                                                <th class="goods-page-total" colspan="2">Total</th>
                                            </tr>
                                            <tr>
                                                <td class="goods-page-image">
                                                    <a href="javascript:;"><img src="{{ asset('images/merchandise/shirtRed_13032019.png') }}" alt="Red Shirt"></a>
                                                </td>
                                                <td class="goods-page-description">
                                                    <h3><a href="javascript:;">Official Tirpentwys Trails Red Shirt</a></h3>
                                                    <em>Size: S</em>
                                                    <em>Quantity: 6</em>
                                                </td>
                                                <td class="goods-page-quantity">
                                                    <div class="product-quantity">
                                                        <input id="product-quantity" type="text" value="1" readonly class="form-control input-sm">
                                                    </div>
                                                </td>
                                                <td class="goods-page-price">
                                                    <strong><span>$</span>47.00</strong>
                                                </td>
                                                <td class="goods-page-total">
                                                    <strong><span>$</span>47.00</strong>
                                                </td>
                                                <td class="del-goods-col">
                                                    <a class="del-goods" href="javascript:;">&nbsp;</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="shopping-total">
                                        <ul>
                                            <li>
                                                <em>Sub total</em>
                                                <strong class="price"><span>$</span>47.00</strong>
                                            </li>
                                            <li>
                                                <em>Shipping cost</em>
                                                <strong class="price"><span>$</span>3.00</strong>
                                            </li>
                                            <li class="shopping-total-price">
                                                <em>Total</em>
                                                <strong class="price"><span>$</span>50.00</strong>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a href="{{ url('merchandise') }}" class="btn btn-default">Continue shopping <i class="fa fa-shopping-cart"></i> </a>
                                <button class="btn btn-primary" type="submit">Checkout <i class="fa fa-check"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

@endsection