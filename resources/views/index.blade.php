@extends('layouts.main')

@section('content')
    <section id="main-slider" class="no-margin">
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1 fade-down">Welcome to Tirpentwys Trails</h2><br />
                                    <p class="boxed animation animated-item-2 fade-up">The place to mountain bike in the heart of Pontypool</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->

<div class="clearfix"></div>
    <section id="testimonial-carousel" class="divider-section">
        <div class="gap"></div>
        <div class="container">
            <div class="row">
                <div class="center gap fade-down section-heading">
                    <h2 class="main-title">Merchandise</h2>
                    <hr>
                    <p>Current 2019 merchandise on offer</p>
                    <div class="gap"></div>
                </div>
                <div class='col-md-offset-2 col-md-8 fade-up'>
                    <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                        <!-- Bottom Carousel Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#quote-carousel" data-slide-to="1"></li>
                            <li data-target="#quote-carousel" data-slide-to="2"></li>
                            <li data-target="#quote-carousel" data-slide-to="3"></li>
                            <li data-target="#quote-carousel" data-slide-to="4"></li>
                            <li data-target="#quote-carousel" data-slide-to="5"></li>
                            <li data-target="#quote-carousel" data-slide-to="6"></li>
                        </ol>
                        <!-- Carousel Slides / Quotes -->
                        <div class="carousel-inner">
                            <?php $a = 0; ?>
                            @foreach ( $merchandise as $merch )
                            <div class="item @if ($a == 0) active @endif">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <img src="{{ $merch->img_source }}" style="height:350px;width:350px;">
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <?php $a++; ?>
                            @endforeach
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="single-page-slider" class="no-margin grey">
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center gap fade-down section-heading">
                                    <h2 class="main-title">What are people saying?</h2>
                                    <hr>
                                    <p>" {{ $review[0]->review }} "</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->
@endsection