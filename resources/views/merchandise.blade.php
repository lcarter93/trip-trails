@extends('layouts.main')

@section('content')

    <section id="single-page-slider" class="no-margin">
        <div class="gap"></div>
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center gap fade-down section-heading">
                                    <h2 class="main-title">Merchandise</h2>
                                    <hr>
                                    <p>All 2019 merchandise available below</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->

    <section id="about-us" class="white">
        <div class="container">
            <div id="meet-the-team" class="row">
                <?php $inc = 0; ?>
                @foreach ( $merchandises as $merchandise )
                    <?php $inc++; ?>
                    <div class="col-md-3 col-xs-6">
                        <div class="center">
                            <div class="team-image">
                                <img class="img-responsive img-thumbnail bounce-in" src="{{ $merchandise->img_source }}" alt="">
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="{{ $merchandise->img_source }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                            <div class="team-content fade-up">
                                <h5>
                                    {{ $merchandise->name }}
                                    <small class="role muted">
                                        £{{ $merchandise->price }}
                                    </small>
                                </h5>
                                <div class="gap"></div>
                                <strong>Select Size:</strong>
                                <select id="select_{{ $inc }}" class="selectedItem" name="selectItemSize">
                                    @foreach($merchandise->stockSizes() as $sizeId => $stock)
                                        <option value="{{ $stock['id'] }}">{{ $stock['name'] }}</option>
                                    @endforeach
                                </select>
                                <br />
                                <strong>Quantity:</strong>
                                <select id="qty_{{ $inc }}" name="selectItemQuantity">
                                    @foreach($merchandise->stockSizes() as $sizeId => $stock)
                                        @for ($i = 1; $i <= $stock['stock']; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor;
                                        @break;
                                    @endforeach
                                </select>
                                <div class="gap"></div>
                                <div id="addToBasket" onClick="addToBasket({{ $inc }})" class="btn btn-outlined btn-primary" data-filter=".wordpress">Add to Basket</div>
                            </div>
                        </div>
                    </div>
                    <?php $inc++; ?>
                @endforeach
            </div><!--/#meet-the-team-->
            <div class="gap"></div>
        </div>
    </section>



@endsection