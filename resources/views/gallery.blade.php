@extends('layouts.main')

@section('content')

    <section id="single-page-slider" class="no-margin">
        <div class="gap"></div>
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center gap fade-down section-heading">
                                    <h2 class="main-title">Gallery</h2>
                                    <hr>
                                    <p>A collection of pictures up Tirpentwys</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->

    <div id="content-wrapper">
        <section id="portfolio" class="white">
            <div class="container">
                <ul class="portfolio-items col-3 isotope fade-up">

                    @foreach ( $gallery as $asset )
                        <li class="portfolio-item apps isotope-item">
                            <div class="item-inner">
                                <img src="{{ $asset->img_source }}" alt="">
                                <h5>{{ $asset->description }}</h5>
                                <div class="overlay">
                                    <a class="preview btn btn-outlined btn-primary" href="{{ $asset->img_source }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                        </li><!--/.portfolio-item-->
                    @endforeach
                </ul>
            </div>
        </section>
    </div>

@endsection