@extends('layouts.main')

@section('content')

    <section id="single-page-slider" class="no-margin">
        <div class="gap"></div>
        <div class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="center gap fade-down section-heading">
                                    <h2 class="main-title">Contact Us</h2>
                                    <hr>
                                    <p>Please Contact us using the form below</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
    </section><!--/#main-slider-->

    <section id="services" class="white">
        <div class="container">
            <div id="content-wrapper">
                <section id="contact" class="white">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-4 fade-up">
                                <h3>Contact Information</h3>
                                <p><span class="icon icon-home"></span>
                                    Tirpentwys Mountain Bike Trails<br/>
                                    Pantygasseg Road<br/>
                                    Pontypool<br />
                                    NP4 6UL<br />
                                    <div class="gap"></div>
                                    <span class="icon icon-mobile"></span>07989 551607<br/>
                                    <span class="icon icon-envelop"></span> <a href="#">contact-us@tirpentwystrails.com</a>
                                </p>
                            </div><!-- col -->

                            <div class="col-md-8 fade-up">
                                <h3>Drop Us A Message</h3>
                                <br>
                                <br>
                                <div id="message"></div>
                                @if(isset($sentMessage) && $sentMessage)
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Success!</strong> Message sent
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Error:</strong> Please fill in all fields!
                                    </div>
                                @endif
                                <form method="post" action="{{ route('contact-us') }}" id="contactform">
                                    {{ csrf_field() }}
                                    <input type="text" name="name" id="name" placeholder="Name" />
                                    <input type="text" name="email" id="email" placeholder="Email" />
                                    <input type="text" name="subject" id="email" placeholder="Subject" />
                                    <textarea name="message" id="comments" placeholder="Message"></textarea>
                                    <input class="btn btn-outlined btn-primary" type="submit" name="submit" value="Submit" />
                                </form>
                            </div><!-- col -->
                        </div><!-- row -->
                        <div class="gap"></div>
                    </div>
                </section>
            </div>

        </div>
    </section>

    <div id="mapwrapper">
        <div id="map"></div>
    </div>




@endsection