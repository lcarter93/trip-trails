$( document ).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/basket/get-basket-session',
        type: 'PUT',
        dataType: 'json',
        success: function(data) {
            var pathname = window.location.pathname;
            if (pathname !== '/basket') {
                if (data > 0) {
                    $("#basket-total").text('£' + data);
                    $(".shop-basket__container").show();
                }
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
});

function addToBasket(selectId){
    var stockItemId = $('#select_' + selectId +' :selected').val();
    var qtyAmt = $('#qty_' + selectId +' :selected').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/basket/store-basket-items',
        type: 'PUT',
        dataType: 'json',
        data: {
            selectStockId: stockItemId,
            selectQuantity: qtyAmt,
        },
        success: function() {
            location.reload(true);
        },
        error: function(response) {
            console.log(response);
        }
    });
}